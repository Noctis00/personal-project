# Personal project

This is my personal git repo with my personal project (done during free time).


Notebooks description:
*  An implementation of LDA from scratch with ONLY numpy ;)
*  A pokemon generator with Linear Network
*  Another pokemon generator with Convolution Network
*  A one-shot learning algorithm to recognize whales


All these projects are still on-going.